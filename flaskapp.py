import os
import sqlite3 as lite
import sys
import json
from urllib2 import Request, urlopen, URLError

from datetime import datetime
from flask import Flask, request, flash, url_for, redirect, \
     render_template, abort, send_from_directory

def getapijoke():
    request = Request('http://api.icndb.com/jokes/random')

    try:
        response = urlopen(request)
        returned = response.read()
        joke = json.loads(returned)
        return str(joke['value']['joke'])
    except URLError, e:
        return 'No kittez. Got an error code:', e

app = Flask(__name__)
app.config.from_pyfile('flaskapp.cfg')

@app.route('/')
def index():
    joke = getapijoke()
    return render_template('jokes.html', joke=joke)

@app.route('/<path:resource>')
def serveStaticResource(resource):
    return send_from_directory('static/', resource)

@app.route("/test")
def test():
    return "<strong>It's Alive!</strong>"

if __name__ == '__main__':
    app.run()
